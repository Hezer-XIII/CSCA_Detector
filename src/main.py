def warn(*args, **kwargs):
	pass
import warnings
warnings.warn = warn

import numpy as np
import os
import SCAAnalyser.Plot as Plot
import SCADetector.Detector as Detector
import SCADetector.PID_reader as PID_reader
import SCADetector.ModelSet as ModelSet
import SCAAnalyser.Overhead as Overhead
import utils.SPEC_Benchmark as bench
from utils.utils import attacks_dict, attacks_dict_color, avail_PAPI_events, timing
import time
import csv
from pypapi import events as papi_events

# supervised models
from sklearn.svm import LinearSVC as SVM
from sklearn.tree import DecisionTreeClassifier as DT
from sklearn.ensemble import RandomForestClassifier as RF
from sklearn.neighbors.nearest_centroid import NearestCentroid as NC
from sklearn.dummy import DummyClassifier as Dummy
from sklearn.linear_model import Perceptron
from sklearn.neural_network import MLPClassifier as NeuralNetwork
from sklearn.linear_model import LogisticRegression as LR
from sklearn.discriminant_analysis import LinearDiscriminantAnalysis as LDA
from sklearn.discriminant_analysis import QuadraticDiscriminantAnalysis as QDA
from sklearn.svm import LinearSVC as SVM
from sklearn.cluster import KMeans
from sklearn.neighbors import KNeighborsClassifier as KNN
from sklearn.naive_bayes import GaussianNB as NB
from sklearn.ensemble import VotingClassifier

# outlier models
from sklearn.svm import OneClassSVM as outSVM

def find_linux_proc_banner():
    pass


if __name__ == '__main__':

    if not os.path.exists("data"):
        os.makedirs("data")
    if not os.path.exists("dist"):
        os.makedirs("dist")
    if not os.path.exists("data/Confusion Matrix"):
        os.makedirs("data/Confusion Matrix")
    if not os.path.exists("data/models"):
        os.makedirs("data/models")
    if not os.path.exists("data/set"):
        os.makedirs("data/set")
    if not os.path.exists("data/set/"+timing):
        os.makedirs("data/set/"+timing)
    if not os.path.exists("data/graph"):
        os.makedirs("data/graph")
    if not os.path.exists("data/graph/"+timing):
        os.makedirs("data/graph/"+timing)



    # Selected HPC
    # See src/utils/utils.py for complete list of HPC
    features = [
                #"Level 2 load misses",
                #"Level 3 total cache reads",
                #"Total cycles",
                #"Level 3 total cache accesses"

                "Level 1 data cache misses",
                "Level 3 total cache accesses",
                "Level 3 total cache misses",
                "Total cycles"
                ]
    # Init the reader object with selected HPCs
    rd = PID_reader.PID_reader([ avail_PAPI_events[ev] for ev in features ])

    # Init the set(s) of models
    # Best Models for targetted detection
    DT_MS   = DT(max_depth=6)
    RF_MS   = RF(n_estimators=2, max_depth=7)
    QDA_MS  = QDA(reg_param=0.8)
    KNN_MS  = KNN(weights="uniform", n_neighbors=4)
    NN_MS   = NeuralNetwork()


    RF_WHISPER   = RF(n_estimators=10,  max_depth=2, random_state=0)
    DT_WHISPER   = DT(max_depth=5)
    SVM_WHISPER  = SVM(random_state=42)
    ENS_WHISPER  = VotingClassifier(estimators=[('SVM', SVM_WHISPER), ('DT', DT_WHISPER), ('RF', RF_WHISPER)], voting='hard') # Not work with "predict_proba"

    """
    ml1  = ModelSet.ModelSet(rd,        # HPC reader
                             [DT_MS, RF_MS, QDA_MS, KNN_MS],      # List of used models
                             features,  # Selected HPCs
                             [
                              "Selected_HPC+PPfull","Selected_HPC+FFfull","Selected_HPC+FRfull"
                             ], # Attacking data set name (data set store in "data/set/[dataSetName]")
                             [
                              "Selected_HPC_NoAttack","Selected_HPC_PPNoatt","Selected_HPC_stress_m","Selected_HPC_stress_i","Selected_HPC_stress_c","Selected_HPC_stress_d"
                              #'stress_c', 'stress_d', 'stress_i', 'stress_m'
                             ], # First non-attacking data set name (data set store in "data/set/[dataSetName]")
                             [
                              #'No_Attack2'
                             ], # Second non-attacking data set name (no difference from the first one, used if 33/33/33 or 50/0/50 mode is needed (see report))
                             clean=False, # True for removing all already trained models
                             set_id = "MS1")

    ml2  = ModelSet.ModelSet(rd, [NN_MS], features,  ["Selected_HPC+PPfull","Selected_HPC+FFfull","Selected_HPC+FRfull"],
                                                     ["Selected_HPC_NoAttack","Selected_HPC_PPNoatt","Selected_HPC_stress_m","Selected_HPC_stress_i","Selected_HPC_stress_c","Selected_HPC_stress_d"],
                                                     [], clean=False, set_id="MS2")
    """
    data_set_attack   = [
                         "Selected_HPC+PPfull","Selected_HPC+FFfull","Selected_HPC+FRfull",
                         "Selected_HPC+PPhalf","Selected_HPC+FFhalf","Selected_HPC+FRhalf"
                         ]
    data_set_noattack = ["Selected_HPC_stress_m","Selected_HPC_stress_i","Selected_HPC_stress_c","Selected_HPC_stress_d"]#,"Selected_HPC_PPNoatt","Selected_HPC_NoAttack",
    ml1  = ModelSet.ModelSet(rd, [RF_WHISPER, DT_WHISPER, SVM_WHISPER], features,  data_set_attack,
                                                                                   data_set_noattack,
                                                                                   [], clean=False, set_id="WHISPER")
    ml2  = ModelSet.ModelSet(rd, [RF_WHISPER], features,  data_set_attack,
                                                          data_set_noattack,
                                                          [], clean=False, set_id="WHISPER")
    ml3  = ModelSet.ModelSet(rd, [DT_WHISPER], features,  data_set_attack,
                                                          data_set_noattack,
                                                          [], clean=False, set_id="WHISPER")
    ml4  = ModelSet.ModelSet(rd, [SVM_WHISPER], features, data_set_attack,
                                                          data_set_noattack,
                                                          [], clean=False, set_id="WHISPER")
    ml5  = ModelSet.ModelSet(rd, [ENS_WHISPER], features, data_set_attack,
                                                          data_set_noattack,
                                                          [], clean=False, set_id="WHISPER_ENS")

    # Create all data set
    """
    # Global HPC Data Set
    ml1.create_dataset()
    ml1.create_dataset(process=['./exec.sh', 'PPfull'], name="Global_HPC+PPfull", label=1)
    ml1.create_dataset(process=['./exec.sh', 'PPhalf'], name="Global_HPC+PPhalf", label=1)
    ml1.create_dataset(process=['./exec.sh', 'FFfull'], name="Global_HPC+FFfull", label=1)
    ml1.create_dataset(process=['./exec.sh', 'FFhalf'], name="Global_HPC+FFhalf", label=1)
    ml1.create_dataset(process=['./exec.sh', 'FRfull'], name="Global_HPC+FRfull", label=1)
    ml1.create_dataset(process=['./exec.sh', 'FRhalf'], name="Global_HPC+FRhalf", label=1)
    ml1.create_dataset(process=['./exec.sh', 'spectre'], name="Global_HPC+Spectre", label=1)
    ml1.create_dataset(process=['./dist/meltdown', 'ffffffff81e00060', '100'], name="Global_HPC+Meltdown", label=1)
    #"""

    # Selected HPC Data Set
    #"""
    #ml1.create_dataset(process=['./dist/meltdown', 'ffffffff81e00060', '100'], name="Selected_HPC+meltdown", label=1, glbl=False)
    #ml1.create_dataset(process=['./exec.sh', 'spectre'], name="Selected_HPC+spectre", label=1, glbl=False)
    #ml1.create_dataset(process=['./exec.sh', 'PPfull'], name="Selected_HPC+PPfull", label=1, glbl=False)
    #ml1.create_dataset(process=['./exec.sh', 'PPhalf'], name="Selected_HPC+PPhalf", label=1, glbl=False)
    #ml1.create_dataset(process=['./exec.sh', 'FRfull'], name="Selected_HPC+FRfull", label=1, glbl=False)
    #ml1.create_dataset(process=['./exec.sh', 'FRhalf'], name="Selected_HPC+FRhalf", label=1, glbl=False)
    #ml1.create_dataset(process=['./exec.sh', 'FFfull'], name="Selected_HPC+FFfull", label=1, glbl=False)
    #ml1.create_dataset(process=['./exec.sh', 'FFhalf'], name="Selected_HPC+FFhalf", label=1, glbl=False)
    #ml1.create_dataset(process=['./exec.sh', 'PPNo'], name="Selected_HPC_PPNoatt", label=0, glbl=False)
    #ml1.create_dataset(process=['stress', '-m', '1'],name="Selected_HPC_stress_m", label=0, glbl=False)
    #ml1.create_dataset(process=['stress', '-i', '1'],name="Selected_HPC_stress_i", label=0, glbl=False)
    #ml1.create_dataset(process=['stress', '-c', '1'],name="Selected_HPC_stress_c", label=0, glbl=False)
    #ml1.create_dataset(process=['stress', '-d', '1'],name="Selected_HPC_stress_d", label=0, glbl=False)
    #"""
    #p = Plot.Plot(features)
    #p.plot_histo(["Selected_HPC_stress_d", "Selected_HPC+spectre"],  0, "Spectre", ["No Attack", "Spectre"])
    #p.plot_histo(["Selected_HPC_stress_d", "Selected_HPC+meltdown"], 1, "Meltdown", ["No Attack", "Meltdown"])
    ml1.fit()
    ml2.fit()
    ml3.fit()
    ml4.fit()
    ml5.fit()


    o2 = Overhead.Overhead(ml2, rd)
    o3 = Overhead.Overhead(ml3, rd)
    o4 = Overhead.Overhead(ml4, rd)
    o5 = Overhead.Overhead(ml5, rd)

    print("SPECTRE  Overhead==")
    #print("Overhead of ", "RF  ", "==>", o2.overhead_calculation(['./exec.sh','spectre']))
    #print("Overhead of ", "DT  ", "==>", o3.overhead_calculation(['./exec.sh','spectre']))
    #print("Overhead of ", "SVM ", "==>", o4.overhead_calculation(['./exec.sh','spectre']))
    #print("Overhead of ", "ENS ", "==>", o5.overhead_calculation(['./exec.sh','spectre']))
    print("MELTDOWN Overhead==")
    print("Overhead of ", "RF  ", "==>", o2.overhead_calculation(['./dist/meltdown', 'ffffffff81e00060', '40']))
    print("Overhead of ", "DT  ", "==>", o3.overhead_calculation(['./dist/meltdown', 'ffffffff81e00060', '40']))
    print("Overhead of ", "SVM ", "==>", o4.overhead_calculation(['./dist/meltdown', 'ffffffff81e00060', '40']))
    print("Overhead of ", "ENS ", "==>", o5.overhead_calculation(['./dist/meltdown', 'ffffffff81e00060', '40']))

    d = Detector.Detector(rd,   # HPC reader
                          [ml1,ml2,ml3,ml4,ml5],    # List of model set
                          glbl=False
                          )

    #with open('data/set/'+timing+'/No_Attack2.csv', mode='w') as set:
    #    set_writer = csv.writer(set, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
    #    set_writer.writerow(["Processus name"] + features + (["Label"]))


    # Create plot
    """
    p = Plot.Plot(features)
    p.plot_histo(["Global_HPC", "Global_HPC+PPfull"],   0, "Prime+Probe_FullKey", ["No Attack", "Prime+Probe_FullKey"])
    p.plot_histo(["Global_HPC", "Global_HPC+PPhalf"],   1, "Prime+Probe_HalfKey", ["No Attack", "Prime+Probe_HalfKey"])
    p.plot_histo(["Global_HPC", "Global_HPC+FRfull"],   2, "Flush+Flush_FullKey", ["No Attack", "Flush+Flush_FullKey"])
    p.plot_histo(["Global_HPC", "Global_HPC+FRhalf"],   3, "Flush+Flush_HalfKey", ["No Attack", "Flush+Flush_HalfKey"])
    p.plot_histo(["Global_HPC", "Global_HPC+FFfull"],   4, "Flush+Reload_FullKey", ["No Attack", "Flush+Reload_FullKey"])
    p.plot_histo(["Global_HPC", "Global_HPC+FFhalf"],   5, "Flush+Reload_HalfKey", ["No Attack", "Flush+Reload_HalfKey"])
    p.plot_histo(["Global_HPC", "Global_HPC+Spectre"],  6, "Spectre", ["No Attack", "Spectre"])
    p.plot_histo(["Global_HPC", "Global_HPC+Meltdown"], 7, "Meltdown", ["No Attack", "Meltdown"])
    """

    # Run the detector tool
    """
    res = True
    proc = bench.no_load()
    mode = "NL"
    d.start(mode)

    while res:
        val = input()
        if val == "stop" :
            res = False
        if val == "mode":
            bench.kill_proc(proc)
            d.stop()
            print("NL ; AL ; FL ?")
            val = input()
            if val == "NL":
                mode = "NL"
                bench.no_load()
            elif val == "AL":
                mode = "AL"
                bench.avg_load()
            elif val == "FL":
                mode = "FL"
                bench.full_load()
            else:
                mode = "NL"
                print("No valid mode, No Load activated by default")
                bench.no_load()
            time.sleep(1)
            d.start(mode)



    d.stop()
    bench.kill_proc(proc)
    bench.clean()
    #"""
