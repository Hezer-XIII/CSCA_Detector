from pypapi import papi_high
from pypapi import papi_low
from pypapi import events as papi_events
from utils.utils import avail_PAPI_events,timing
import pypapi
import os
import signal
import time
import psutil

class PID_reader(object):
    """docstring for PID_reader."""
    def __init__(self, chosen_events):
        """
        @param  chosen_events   List of chosen PAPI events
        """
        super(PID_reader, self).__init__()
        self.chosen_events = chosen_events

    def events_size(self, events=None):
        if events==None:
            events = self.chosen_events
        return len(self.chosen_events)

    def set_events(self, new_events):
        self.chosen_events = new_events

    def init_papi(self):
        papi_high.start_counters(self.chosen_events)

    def stop_papi(self):
        _ = papi_high.stop_counters()
        print("PAPI stop")

    def read_papi_events(self, pid=None, events=None):
        if events==None:
            events = self.chosen_events
        if pid==None: #Global HPC
            return self._read_papi_events_global(events)
        else: #Targetted HPC
            return self._read_papi_events_targetted(pid, events)

    def _read_papi_events_global(self, events):
        #papi_high.start_counters(events)
        _ = papi_high.read_counters() # reset counters
        time.sleep(float(timing))
        return papi_high.read_counters()

    def _read_papi_events_targetted(self, pid, events):
        papi_low.library_init()

        try:
            _ = psutil.Process(pid)
            Event_Set = papi_low.create_eventset()
            for event in events:
                papi_low.add_event(Event_Set, event)

            papi_low.attach(Event_Set, pid)

            papi_low.start(Event_Set)

            time.sleep(float(timing))

            results = papi_low.stop(Event_Set)
            #print("Process "+str(pid)+" : ",result)

            papi_low.detach(Event_Set)
            papi_low.cleanup_eventset(Event_Set)
            papi_low.destroy_eventset(Event_Set)

            return results
        except (pypapi.exceptions.PapiInvalidValueError, psutil.NoSuchProcess, pypapi.exceptions.PapiCountError):
            raise psutil.NoSuchProcess(-1)

    def read_all_papi_events(self, pid, papi_list):
        data = []
        try:
            for e in papi_list:
                data += self.read_papi_events(pid, events=[avail_PAPI_events[e]])
            return data
        except psutil.NoSuchProcess:
            raise psutil.NoSuchProcess(-1)
