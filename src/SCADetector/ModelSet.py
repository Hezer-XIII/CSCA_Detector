import numpy as np
import pandas as pd
import random
import csv
import subprocess
import psutil
import os
import time
import pickle
from sklearn.model_selection import cross_val_score
from utils.utils import non_attack_process, avail_PAPI_events, attacks_dict, timing
from SCAAnalyser.ConfusionMatrix import ConfusionMatrix

def warn(*args, **kwargs):
	pass
import warnings
warnings.warn = warn

class ModelSet(object):
    """docstring for ModelSet."""

    def __init__(self, reader, clfs, features, dataSetA, dataSetNA=[], dataSetNA2=[], outlier=False, clean=False, set_id=""):
        """
        @param  reader      An object used to read PAPI events
        @param  clfs        A list of Machine Learning models (ex: [ SVM(), DT() ] )
        @param  dataSetA    A list of Attack data set name in str format (ex: [ 'spectre', 'meltdown' ])
        @param  dataSetNA   A list of No-Attack data set name in str format (ex: [ 'stress_c', 'stress_d' ])
        @param  outlier     A boolean. Outlier = True mean the i-th list of models is a list of outlier detection models
        @param  clean       A boolean, True mean all already trained models will be erased and re-trained
        """
        super(ModelSet, self).__init__()
        self.reader = reader
        self.clfs = clfs
        self.features = features
        self.dataSetA  = dataSetA
        self.dataSetNA = dataSetNA
        self.dataSetNA2= dataSetNA2
        self.outlier = outlier
        self.dataSet_size = 200000
        self.has_fit = False
        self.set_id = set_id
        self.confusion_matrixs = ConfusionMatrix()                           # 1 CM
        if clean:
            clean_models()

    def __str__(self):
        return "Models : " + str([ clf.__class__.__name__ for clf in self.clfs ]) + "\nAttack used to train : " + str(self.dataSetA) + "\nNo-Attack used to train : " + str(self.dataSetNA) + "\n"

    def str_models(self,i):
        return str([self.clfs[i].__class__.__name__])+ " Trained with: " + str(self.dataSetA)

    def size(self):
        return len(self.clfs)

    def _process_pid(p_name, FNULL):
        if p_name == "*":
            pids = []
            for proc in psutil.process_iter(attrs=['pid','name']):
                if proc.info['name'] in non_attack_process:
                    pids += [proc]
            proc = [ random.choice( pids ) ]
        else:
            proc = [ psutil.Process(subprocess.Popen(p_name,
                                    stdout=FNULL, stderr=subprocess.STDOUT).pid) ]
            time.sleep(0.01)
            while proc[-1].children():
                proc += [proc[-1].children()[0]]
        return proc

    def create_dataset(self, process=None, name="Global_HPC", label=0, glbl=True):
        """
        @param  name            Name of the data set
        @param  process         Complet process in subprocess format
                                (e.g. ['stress', '-c' '1'] )
                                Mandatori if glbl=False
        @param  label           Label of the process. 0 for no attack, 1 for attack
        @param  glbl            If True, create a data set of global HPC
                                If False, create a data set of specific HPC (see process parameter)
                                Default is True
        """
        if glbl==False:
            self._create_dataset_targetted(process, name, label)
        else:
            self._create_dataset_global(process, name, label)

    def _create_dataset_global(self, process, name, label):
        """
        @param  name            Name of the data set
        @param  process         Complet process in subprocess format
                                (e.g. ['stress', '-c' '1'] )
                                Mandatori if glbl=False
        @param  label           Label of the process.
                                0 if no attack run in the system
                                1 if one attack run in the system
        """
        print("Start new Data Set for", name)
        FNULL = open(os.devnull, 'w')
        #papi_list = list(avail_PAPI_events.keys())     #all events
        #papi_list = [ (name if val in self.reader.chosen_events else '') for name,val in avail_PAPI_events.items() ]     #selected events
        papi_list = []
        for chosen in self.reader.chosen_events:
            for n,val in avail_PAPI_events.items():
                if val==chosen:
                    papi_list.append(n)

        self.reader.init_papi()
        with open('data/set/'+timing+'/'+name+'.csv', mode='w') as set:
            set_writer = csv.writer(set, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
            set_writer.writerow(papi_list + (["Label"]))
            actual_size = 0
            data = []
            proc = [1]
            while (actual_size < self.dataSet_size):
                try:
                    if process != None:
                        _ = psutil.Process(proc[-1].pid)
                        #proc = ModelSet._process_pid(process, FNULL)
                    data = self.reader.read_papi_events()#, events=[ avail_PAPI_events[e] for e in papi_list ])                   #selected events
                    #data = self.reader.read_all_papi_events(proc[-1].pid, papi_list)   #all events
                    if True in [ i != 0 for i in data ]:
                        set_writer.writerow(data + [label] )
                        #print(data)
                        actual_size += 1
                    if actual_size%100 == 0:
                        print(str(actual_size) + "/"+ str(self.dataSet_size))
                except (AttributeError, psutil.NoSuchProcess):
                    print("(re)-run process")
                    proc = ModelSet._process_pid(process, FNULL)
                    print(proc)
            for p in proc:
                try:
                    p.kill()
                except (AttributeError):
                    pass
        FNULL.close()
        self.reader.stop_papi()

    def _create_dataset_targetted(self, process, name, label):
        """
        @param  name            Name of the process
        @param  process         Complet process in subprocess format
                                (e.g. ['stress', '-c' '1'] )
        @param  label           Label of the process. 0 for no attack, 1 for attack
        """
        print("Start new Data Set for", name)
        FNULL = open(os.devnull, 'w')
        #papi_list = list(avail_PAPI_events.keys())     #all events
        #papi_list = [ (name if val in self.reader.chosen_events else '') for name,val in avail_PAPI_events.items() ]     #selected events
        papi_list = []
        for chosen in self.reader.chosen_events:
            for n,val in avail_PAPI_events.items():
                if val==chosen:
                    papi_list.append(n)

        with open('data/set/'+timing+'/'+name+'.csv', mode='w') as set:
            set_writer = csv.writer(set, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
            set_writer.writerow(["Processus name"] + papi_list + (["Label"]))
            actual_size = 0
            data = []
            proc = [1]
            while (actual_size < self.dataSet_size):
                try:
                    #proc = ModelSet._process_pid(process, FNULL)
                    data = self.reader.read_papi_events(pid=proc[-1].pid)#, events=[ avail_PAPI_events[e] for e in papi_list ])                   #selected events
                    #data = self.reader.read_all_papi_events(proc[-1].pid, papi_list)   #all events
                    if True in [ i != 0 for i in data ]:
                        set_writer.writerow([proc[-1].name()] + data + [label] )
                        actual_size += 1
                    if actual_size%100 == 0:
                        print(str(actual_size) + "/"+ str(self.dataSet_size))
                except (AttributeError, psutil.NoSuchProcess):
                    print("(re)-run process")
                    proc = ModelSet._process_pid(process, FNULL)
                    print(proc)
            for p in proc:
                p.kill()
        FNULL.close()

    def create_dataset_noattack(self):
        self.create_dataset("*", "No_Attack", 0, glbl=False)

    def fit(self):
        for i in range(len(self.clfs)):
            if os.path.isfile('data/models/'+self.clfs[i].__class__.__name__+"__"+str(self.dataSetA)+"_"+self.set_id+'.ml'):
                print ("already fit")
                self.clfs[i] = pickle.load(open('data/models/'+self.clfs[i].__class__.__name__+"__"+str(self.dataSetA)+"_"+self.set_id+'.ml', 'rb'))
            else:
                if self.outlier:
                    self._train_model(self.clfs[i], self.dataSetA)
                else:
                    self._train_model(self.clfs[i], self.dataSetA, self.dataSetNA, self.dataSetNA2)
        self.has_fit = True

    def _train_model(self, clf, X_names=[], Y_names=[], Z_names=[]):
        # Attack Data Set
        X_train = np.empty((0,self.reader.events_size()+1))
        X_test  = np.empty((0,self.reader.events_size()+1))
        for name in X_names:
            data = pd.read_csv('data/set/'+timing+'/'+name+'.csv')[self.features+['Label']]
            X_train = np.concatenate((X_train, np.array(data[10000:])))
            X_test  = np.concatenate((X_test, np.array(data[:10000])))
        np.random.shuffle(X_train)
        np.random.shuffle(X_test)
        X_train = X_train[:100000]
        X_test  = X_test[:10000]
        att_len = len(X_train)
        # Stress Data Set
        Y_train = np.empty((0,self.reader.events_size()+1))
        Y_test  = np.empty((0,self.reader.events_size()+1))
        for name in Y_names:
            data = pd.read_csv('data/set/'+timing+'/'+name+'.csv')[self.features+['Label']]
            Y_train = np.concatenate((X_train, np.array(data[10000:])))
            Y_test  = np.concatenate((Y_test, np.array(data[:10000])))
        np.random.shuffle(Y_train)
        np.random.shuffle(Y_test)
        Y_train = Y_train[:100000]
        Y_test  = Y_test[:10000]
        noatt_len = len(X_train) - att_len
        # No Attack Data Set
        Z_train = np.empty((0,self.reader.events_size()+1))
        Z_test  = np.empty((0,self.reader.events_size()+1))
        for name in Z_names:
            data = pd.read_csv('data/set/'+timing+'/'+name+'.csv')[self.features+['Label']]
            Z_train = np.concatenate((Z_train, np.array(data[10000:])))
            Z_test  = np.concatenate((Z_test, np.array(data[:10000])))
        np.random.shuffle(Z_train)
        np.random.shuffle(Z_test)
        Z_train = Z_train[:100000]
        Z_test  = Z_test[:10000]

        #if not self.outlier:
        #    Y_train = np.concatenate((np.array([[1]]*  att_len), np.array([[0]]*noatt_len)))

        #if not self.outlier:
        #    scores = cross_val_score(clf, X_train, Y_train, cv=5)
        #    print(clf.__class__.__name__, "With "+str(attack_set_name), "    ", scores.mean())
        #_test(clf,X_train, cv=5)
        #_test(clf,X_train, cv=2)
        test  = np.concatenate((X_test, Y_test, Z_test ))
        train = np.concatenate((X_train,Y_train,Z_train))
        clf.fit(train[:,:-1], train[:,-1:])
        print("Score Testing Set ", clf.score(test[:,:-1],test[:,-1:]))
        pickle.dump(clf, open("data/models/"+clf.__class__.__name__+"__"+str(X_names)+"_"+self.set_id+".ml", 'wb'))

    def predict(self, data, p_name=None):
        if not self.has_fit:
            print("ModelSet must be fit")
            return [0]*len(self.clfs)
        res = []
        for clf in self.clfs:
            val = int(clf.predict(data))
            res.append(val)
        """
        for clf, matrix in zip(self.clfs, self.confusion_matrixs):
            val = int(clf.predict(data))
            if val == 1:
                if p_name in attacks_dict.keys():
                    matrix.set_values(0,1,0,0,1,0)
                else:
                    matrix.set_values(0,0,1,0,0,1)
            else:
                if p_name in attacks_dict.keys():
                    print("Misclassify by ", clf.__class__.__name__)
                    matrix.set_values(1,0,0,0,1,0)
                else:
                    matrix.set_values(0,0,0,1,0,1)
            res.append(val)
        """
        if isinstance(p_name, list):
            for proc in p_name:
                tmp = proc.info['name']
                if tmp in attacks_dict.keys():
                    break
            p_name = tmp
        if p_name != None:
            if 1 in res:
                if p_name in attacks_dict.keys():
                    self.confusion_matrixs.set_values(0,1,0,0,1,0)
                else:
                    self.confusion_matrixs.set_values(0,0,1,0,0,1)
            else:
                if p_name in attacks_dict.keys():
                    #print("Misclassify")
                    self.confusion_matrixs.set_values(1,0,0,0,1,0)
                else:
                    self.confusion_matrixs.set_values(0,0,0,1,0,1)
        #"""
        return res

    def is_outlier(self):
        return self.outlier

    def confusion_matrix(self):
        res = ""
        """
        for clf, matrix in zip(self.clfs, self.confusion_matrixs):
            res += '------------------------------'
            res += clf.__class__.__name__ + "\n"
            res += matrix.matrix()
        """
        res += self.confusion_matrixs.matrix()
        #"""
        return res



def _test(clf, X_train, cv=5):
    res = 0
    size = int(len(X_train) / cv)
    data = pd.read_csv('data/set/'+timing+'/No_Attack.csv')
    X_test = np.array(data.drop(['Label'],1))
    for i in range(cv):
        train = np.concatenate((X_train[: i*size ], X_train[(i+1)*size:]))
        test  = X_train[i*size : (i+1)*size]
        clf.fit(train)
        print(clf.predict(test))
        print(clf.predict(X_test))
        res += sum([ (i if i==1 else 0) for i in clf.predict(test) ]) / size
    print(res/cv)

def clean_models():
    """Delete all already fit models"""
    for _,_,files in os.walk("data/models/"):
        for f in files:
            if '.ml' in f:
                os.remove("data/models/"+f)
