import os
import signal
import time
import psutil
import csv
import numpy as np
from pypapi import events as papi_events
from utils.utils import attacks_dict, timing

class Detector(object):
    """docstring for Detector."""
    def __init__(self, reader, trainer, glbl=False):
        """
        @param  reader      An object use to read HPC events
        @param  trainer     A list of Trainer, an object used to trained models and build data set
        @param  glbl        If True, read the global HPC of the system
                            If False, read the HPC of each processes one by one
                            Default is False
        """
        super(Detector, self).__init__()
        self.glbl = glbl
        self.reader = reader
        self.list_of_model_set = trainer
        self.in_run = False
        self.child = 0

    def start(self, mode="NL"):
        if self.in_run:
            print ("Detector already in run")
        else:
            print ("Detector on")
            print ("In "+mode+" mode")
            print ("Write 'stop' to stop the detector tool")
            self.in_run = True
            self.child = os.fork()
            if self.child: #father
                pass
            else: #child
                if self.glbl:
                    self._global_detector(mode)
                else:
                    self._targetted_detector(mode)

    def _global_detector(self, mode="NL"):
        signal.signal(signal.SIGUSR1, self.stop)
        self.reader.init_papi()
        while (self.in_run):
            time.sleep(0.1)
            print("RUN==========")
            start_cycle1 = time.perf_counter()
            start_cycle2 = time.process_time()
            proc_list = [ proc for proc in psutil.process_iter(attrs=['name','pid'])]
                      # Used for debugging or stats
            res = self.reader.read_papi_events()
            if (True in [ i != 0 for i in res ]):
                # Process with all 0 values mustn't be an attack
                if self.detection(res, proc_list):
                    print ("Attack detected")
                    print("================================")
                else:
                    pass
                    #TODO Use to create a non-attacking data set
                    #     /!\ Do not run an attack during this data set creation
                    #print ("No attack")
                    #with open('data/set/'+timing+'/No_Attack2.csv', mode='a') as set:
                    #    set_writer = csv.writer(set, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
                    #    set_writer.writerow([proc.info['name']] + res + [0] )
            with open('data/confusion_matrix'+mode+'.txt', 'w') as f:
                # Confusion Matrix updated every cycle
                f.write(self.confusion_matrix())
            end_cycle1 = time.perf_counter()
            end_cycle2 = time.process_time()
            #print("Time of this Cycle : ", end_cycle1-start_cycle1, end_cycle2-start_cycle2, " (perf counter ; process time)")
        self.reader.stop_papi()

    def _targetted_detector(self, mode="NL"):
        signal.signal(signal.SIGUSR1, self.stop)
        buffer = []
        while (self.in_run):
            print("RUN==========")
            start_cycle1 = time.perf_counter()
            start_cycle2 = time.process_time()
            for proc in psutil.process_iter(attrs=['pid','name']):
                try:
                    if proc.pid != os.getpid():
                        res = self.reader.read_papi_events(proc.pid)
                        if (True in [ i != 0 for i in res ]):
                            # Process with all 0 values mustn't be an attack
                            if self.detection(res, proc.info['name']):
                            #if False:
                                print ("Attack detected : ", proc.info['name'])
                                if proc.info['name'] in attacks_dict.keys():
                                    # For testing, only kill known attacks
                                    proc.terminate()
                                print("================================")
                            else:
                                pass
                                #TODO Use to create a non-attacking data set
                                #     /!\ Do not run an attack during this data set creation
                                #print ("No attack")
                                #if not proc.info['name'] in attacks_dict:
                                #    buffer.append([proc.info['name']] + res + [0])
                                #    if (len(buffer)%10 == 0):
                                #        print(str(len(buffer)),"/",100)
                                #    if len(buffer) >= 100:
                                #        with open('data/set/'+timing+'/Selected_HPC_NoAttack.csv', mode='a') as set:
                                #            set_writer = csv.writer(set, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
                                #            for buf in buffer:
                                #                set_writer.writerow(buf)
                                #            print("BUFFER CLEAN")
                                #            buffer = []
                except psutil.NoSuchProcess:
                    pass#print("Process error!")
                    #print(proc)
            with open('data/confusion_matrix.txt', 'w') as f:
                # Confusion Matrix updated every cycle
                f.write(self.confusion_matrix())
            end_cycle1 = time.perf_counter()
            end_cycle2 = time.process_time()
            #print("Time of this Cycle : ", end_cycle1-start_cycle1, end_cycle2-start_cycle2, " (perf counter ; process time)")

    def _stop_child(self):
        self.in_run = False
        print ("Child stopped")
        time.sleep(1)
        exit()

    def _stop_father(self):
        self.in_run = False
        os.kill(self.child, signal.SIGUSR1)
        time.sleep(1)
        print("the Detector is now off")

    def stop(self, signum=None, stack=None):
        if self.in_run:
            if self.child == 0:
                self._stop_child()
            else:
                self._stop_father()
        else:
            print ("The Detector is already off")

    def detection(self, data, p_name=None):
        is_attack = False
        for model_set in self.list_of_model_set:
            if (1 or "1") in model_set.predict(np.array([data]), p_name):
                is_attack = True
        return is_attack

    def confusion_matrix(self):
        res = "===== Confusion Matrix"
        for model_set in self.list_of_model_set:
            res += "=============================\n"
            res += str(model_set)
            res += model_set.confusion_matrix()
        return res
