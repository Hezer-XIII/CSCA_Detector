from pypapi import events as papi_events
from collections import defaultdict


timing = '0.01' #timing for HPC count


# List of available PAPI event
# See PAPI doc to know which HPC are available on your system
avail_PAPI_events ={
    "Level 1 data cache misses": papi_events.PAPI_L1_DCM,
    "Level 1 instruction cache misses": papi_events.PAPI_L1_ICM,
    "Level 2 data cache misses": papi_events.PAPI_L2_DCM,
    "Level 2 instruction cache misses": papi_events.PAPI_L2_ICM,
    "Level 1 cache misses": papi_events.PAPI_L1_TCM,
    "Level 2 cache misses": papi_events.PAPI_L2_TCM,
    "Level 3 total cache misses": papi_events.PAPI_L3_TCM,
    #"Requests for a snoop": papi_events.PAPI_CA_SNP,
    #"Requests for exclusive access to shared cache line": papi_events.PAPI_CA_SHR,
    #"Requests for exclusive access to clean cache line": papi_events.PAPI_CA_CLN,
    #"Requests for cache line intervention": papi_events.PAPI_CA_ITV,
    "Level 3 load misses": papi_events.PAPI_L3_LDM,
    #"Data translation lookaside buffer misses": papi_events.PAPI_TLB_DM,
    #"Instruction translation lookaside buffer misses": papi_events.PAPI_TLB_IM,
    "Level 1 load misses": papi_events.PAPI_L1_LDM,
    "Level 1 store misses": papi_events.PAPI_L1_STM,
    "Level 2 load misses": papi_events.PAPI_L2_LDM,
    "Level 2 store misses": papi_events.PAPI_L2_STM,
    #"Data prefetch cache misses": papi_events.PAPI_PRF_DM,
    #"Cycles Stalled Wainting for memory writes": papi_events.PAPI_MEM_WCY,
    #"Cycles with no instruction issue": papi_events.PAPI_STL_ICY,
    #"Cycles with maximum instruction issue": papi_events.PAPI_FUL_ICY,
    #"Cycle with no instructions completed": papi_events.PAPI_STL_CCY,
    #"Cycles with maximum instructions completed": papi_events.PAPI_FUL_CCY,
    "Unconditional branch instructions": papi_events.PAPI_BR_UCN,
    "Conditional branch instructions": papi_events.PAPI_BR_CN,
    "Conditional branch instructions taken": papi_events.PAPI_BR_TKN,
    "Conditional branch instructions not taken": papi_events.PAPI_BR_NTK,
    "Conditional branch instructions mispredicted": papi_events.PAPI_BR_MSP,
    "Conditional branch instructions correctly predicted": papi_events.PAPI_BR_PRC,
    #"Instructions completed": papi_events.PAPI_TOT_INS,
    #"Load instructions": papi_events.PAPI_LD_INS,
    #"Store instructions": papi_events.PAPI_SR_INS,
    #"Branch instructions": papi_events.PAPI_BR_INS,
    #"Cycles stalled on my resource": papi_events.PAPI_RES_STL,
    "Total cycles": papi_events.PAPI_TOT_CYC,
    #"Load-store instructions completed": papi_events.PAPI_LST_INS,
    "Level 2 data cache accesses": papi_events.PAPI_L2_DCA,
    "Level 3 data cache accesses": papi_events.PAPI_L3_DCA,
    "Level 2 data cache reads": papi_events.PAPI_L2_DCR,
    "Level 3 data cache reads": papi_events.PAPI_L3_DCR,
    "Level 2 data cache writes": papi_events.PAPI_L2_DCW,
    "Level 3 data cache writes": papi_events.PAPI_L3_DCW,
    "Level 2 instruction cache hits": papi_events.PAPI_L2_ICH,
    "Level 2 instruction cache accesses": papi_events.PAPI_L2_ICA,
    "Level 3 instruction cache accesses": papi_events.PAPI_L3_ICA,
    "Level 2 instruction cache reads": papi_events.PAPI_L2_ICR,
    "Level 3 instruction cache reads": papi_events.PAPI_L3_ICR,
    "Level 2 total cache accesses": papi_events.PAPI_L2_TCA,
    "Level 3 total cache accesses": papi_events.PAPI_L3_TCA,
    "Level 2 total cache reads": papi_events.PAPI_L2_TCR,
    "Level 3 total cache reads": papi_events.PAPI_L3_TCR,
    "Level 2 total cache writes": papi_events.PAPI_L2_TCW,
    "Level 3 total cache writes": papi_events.PAPI_L3_TCW,
    #"Floating point operations": papi_events.PAPI_SP_OPS,
    #"Floating point operations": papi_events.PAPI_DP_OPS,
    #"Single precision vector-SIMD instructions": papi_events.PAPI_VEC_SP,
    #"Double precision vector-SIMD instructions": papi_events.PAPI_VEC_DP,
    #"Reference clock cycles": papi_events.PAPI_REF_CYC
    }


attacks_dict = {
    "Meltdown": ['/home/hezer/Documents/School/ULB/MA2_2018-2019/Master_Thesis/code/dist/meltdown',
                 'ffffffff81e00060', '500'],
    "meltdown": ['/home/hezer/Documents/School/ULB/MA2_2018-2019/Master_Thesis/code/dist/meltdown',
                 'ffffffff81e00060', '500'],
    "Spectre": ['./exec.sh', 'spectre'],
    "Prime+Probe_FullKey": ['./exec.sh', 'PPfull'],
    "Prime+Probe_HalfKey": ['./exec.sh', 'PPhalf'],
    "Flush+Reload_FullKey": ['./exec.sh', 'FRfull'],
    "Flush+Reload_HalfKey": ['./exec.sh', 'FRhalf'],
    "Flush+Flush_FullKey": ['./exec.sh', 'FFfull'],
    "Flush+Flush_HalfKey": ['./exec.sh', 'FFhalf'],
    }

def a():
    return 'g'
attacks_dict_color = defaultdict(a)
attacks_dict_color["Meltdown"] = 'r'
attacks_dict_color["Spectre"] = 'r'#'b'
attacks_dict_color["Prime+Probe_FullKey"] = 'r'#'c'
attacks_dict_color["Prime+Probe_HalfKey"] = 'r'#'c'
attacks_dict_color["Flush+Reload_FullKey"] = 'r'#'m'
attacks_dict_color["Flush+Reload_HalfKey"] = 'r'#'m'
attacks_dict_color["Flush+Flush_FullKey"] = 'r'#'y'
attacks_dict_color["Flush+Flush_HalfKey"] = 'r'#'y'
attacks_dict_color["stress_c"] = 'g'#'y'
attacks_dict_color["stress_d"] = 'b'#'y'
attacks_dict_color["stress_i"] = 'y'#'y'
attacks_dict_color["stress_m"] = 'm'#'y'


non_attack_process = [
    "spotify",
    "vlc",
    "chrome",
    "gnome-shell",
    "Xorg",
    "atom",
    "thunderbird",
    "dropbox",
    "python3",
    "python",
    "firefox",
    "brave",
    "bash",
    "jupyter-noteboo",
    "jupyter-notebook",
]
