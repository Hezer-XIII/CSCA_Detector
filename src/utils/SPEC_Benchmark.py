import os
import subprocess


def clean():
    if os.path.exists('./--quiet'):
        os.remove('./--quiet')
    if os.path.exists('gmon.out'):
        os.remove('gmon.out')
    if os.path.exists('mcf.out'):
        os.remove('mcf.out')
    if os.path.exists('omnetpp.sca'):
        os.remove('omnetpp.sca')
    if os.path.exists('games'):
        os.remove('games')
    if os.path.exists('omnetpp.ini'):
        os.remove('omnetpp.ini')
    if os.path.exists('STATS_Detection.csv'):
        os.remove('STATS_Detection.csv')


def kill_proc(proc_list):
    for proc in proc_list:
        proc.kill()

def no_load():
    print("No Load mode")
    return []

def avg_load():
    print("Avg Load mode")
    FNULL = open(os.devnull, 'w')
    proc1 = subprocess.Popen(['/home/Jeremy/CSCA_Detector/lib/cpu2006/benchspec/CPU2006/429.mcf/exe/mcf_base.gcc43-64bit', '/home/Jeremy/CSCA_Detector/lib/cpu2006/benchspec/CPU2006/429.mcf/data/ref/input/inp.in'], stdout=FNULL, stderr=subprocess.STDOUT)
    proc2 = subprocess.Popen(['/home/Jeremy/CSCA_Detector/lib/cpu2006/benchspec/CPU2006/445.gobmk/exe/gobmk_base.gcc43-64bit', '-o', '--quiet', '--gtp-input', '/home/Jeremy/CSCA_Detector/lib/cpu2006/benchspec/CPU2006/445.gobmk/data/ref/input/13x13.tst'], stdout=FNULL, stderr=subprocess.STDOUT)
    FNULL.close()
    return [proc1, proc2]

def full_load():
    print("Full Load mode")
    FNULL = open(os.devnull, 'w')
    proc1 = subprocess.Popen(['/home/Jeremy/CSCA_Detector/lib/cpu2006/benchspec/CPU2006/429.mcf/exe/mcf_base.gcc43-64bit', '/home/Jeremy/CSCA_Detector/lib/cpu2006/benchspec/CPU2006/429.mcf/data/ref/input/inp.in'], stdout=FNULL, stderr=subprocess.STDOUT)
    proc2 = subprocess.Popen(['/home/Jeremy/CSCA_Detector/lib/cpu2006/benchspec/CPU2006/445.gobmk/exe/gobmk_base.gcc43-64bit', '-o', '--quiet', '--gtp-input', '/home/Jeremy/CSCA_Detector/lib/cpu2006/benchspec/CPU2006/445.gobmk/data/ref/input/13x13.tst'], stdout=FNULL, stderr=subprocess.STDOUT)
    proc3 = subprocess.Popen(['/home/Jeremy/CSCA_Detector/lib/cpu2006/benchspec/CPU2006/471.omnetpp/exe/omnetpp_base.gcc43-64bit'], stdout=FNULL, stderr=subprocess.STDOUT)
    proc4 = subprocess.Popen(['/home/Jeremy/CSCA_Detector/lib/cpu2006/benchspec/CPU2006/483.xalancbmk/exe/Xalan_base.gcc43-64bit', '-v', '/home/Jeremy/CSCA_Detector/lib/cpu2006/benchspec/CPU2006/483.xalancbmk/data/ref/input/t5.xml', '/home/Jeremy/CSCA_Detector/lib/cpu2006/benchspec/CPU2006/483.xalancbmk/data/ref/input/xalanc.xsl'], stdout=FNULL, stderr=subprocess.STDOUT)
    FNULL.close()
    return [proc1, proc2, proc3, proc4]
