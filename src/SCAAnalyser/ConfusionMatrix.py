
class ConfusionMatrix(object):
    """docstring for ConfusionMatrix."""
    def __init__(self):
        super(ConfusionMatrix, self).__init__()
        self.fn = 0 # Attack labelled as non-attack
        self.tn = 0 # Attack labelled as attack
        self.fp = 0 # Non attack labelled as attack
        self.tp = 0 # Non attack labelled as non-attack
        self.attack_counter = 0
        self.no_attack_counter = 0

    def matrix(self):
        res  = "TP : " + str(self.tp / max(1,self.no_attack_counter) * 100.)  + "   FN : " + str(self.fn / max(1,self.attack_counter) * 100.) + "\n"
        res += "FP : " + str(self.fp / max(1,self.no_attack_counter) * 100.)  + "   TN : " + str(self.tn / max(1,self.attack_counter) * 100.) + "\n"
        return res

    def set_values(self,fn, tn, fp, tp, attack_counter, no_attack_counter):
        self.fp += fp
        self.tp += tp
        self.fn += fn
        self.tn += tn
        self.attack_counter += attack_counter
        self.no_attack_counter += no_attack_counter
