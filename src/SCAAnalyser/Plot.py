import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import os
from utils.utils import timing

class Plot(object):
    """docstring for Plot."""
    def __init__(self, chosen_events):
        super(Plot, self).__init__()
        self.chosen_events = chosen_events
        self.color_list  = ['g', 'r', 'b', 'y', 'c', 'm', 'k']
        self.bins = 75

    def plot_histo(self,dataset_list, num, title, attack_name):
        data = []
        for name in dataset_list:
            tmp = pd.read_csv('data/set/'+timing+'/'+name+'.csv')[self.chosen_events]
            data += [np.array(tmp[1:1000])]
        plt.figure(num+1, figsize=(11,7))
        for e,i in zip(self.chosen_events,range(len(self.chosen_events))):
            plt.subplot(2,2,i+1)
            for A,c in zip(data,self.color_list[:len(dataset_list)]):
                plt.hist(A[i], self.bins, alpha=1, histtype='step', color=c, range=(1,100000))
            plt.ylabel('Frequency', fontweight='bold')
            plt.title(e, fontweight='bold')
            plt.legend(attack_name)
        plt.savefig("data/graph/"+timing+"/"+title+".png")
        #plt.show()
