import subprocess
import os
import time
import numpy as np

class Overhead(object):
    """docstring for Overhead."""
    def __init__(self, ml, reader):
        super(Overhead, self).__init__()
        self.ml = ml
        self.reader = reader

    def overhead_calculation(self, attack):
        """Compute the Overhead of running detection on system"""
        FNULL = open(os.devnull, 'w')
        overhead = 0
        overhead2 = 0
        n = 50
        for i in range(n):
            start_cycle1 = time.perf_counter()
            #start_cycle1 = time.process_time()
            proc = subprocess.Popen(attack,stdout=FNULL,stderr=subprocess.STDOUT)
            while proc.poll() is None:
                pass    # Wait until the process is end
            end_cycle1 = time.perf_counter()
            #end_cycle1 = time.process_time()
            time_no_detection = end_cycle1 - start_cycle1


            start_cycle1 = time.perf_counter()
            #start_cycle1 = time.process_time()
            proc = subprocess.Popen(attack,stdout=FNULL,stderr=subprocess.STDOUT)
            self.ml.predict(np.array([self.reader.read_papi_events(pid=proc.pid)]))
            while proc.poll() is None:
                pass    # Wait until the process is end
            end_cycle1 = time.perf_counter()
            #end_cycle1 = time.process_time()
            time_detection = end_cycle1 - start_cycle1

            overhead +=  abs(time_detection - time_no_detection) / time_no_detection * 100.
            overhead2 += (time_detection - time_no_detection) / time_no_detection * 100.
            #print(overhead, overhead2)
        FNULL.close()
        return overhead/n , overhead2/n
