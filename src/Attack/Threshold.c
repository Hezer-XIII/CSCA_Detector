#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#ifdef _MSC_VER
#include <intrin.h>
#pragma optimize("gt", on)
#else
#include <x86intrin.h>
#endif


int main(int argc, char const *argv[]) {
  register uint64_t tmp_time, cached_time=0, uncached_time=0;
  volatile uint8_t *addr;
  uint8_t val;
  int junk = 0;
  int round = 1;
  int tmp = round;

  while(tmp>0){
    /* Put val in cache */
    val = 10;
    val = 10;

    addr = &val;
    tmp_time = __rdtscp(&junk);
    junk = *addr;
    cached_time += (__rdtscp(&junk) - tmp_time);
    usleep(1);
    //printf("%ld\n", cached_time);

    /* Put val out of cache */
    _mm_clflush(&val);
    _mm_clflush(&val);

    addr = &val;
    junk = 0;
    tmp_time = __rdtscp(&junk);
    junk = *addr;
    uncached_time += __rdtscp(&junk) - tmp_time;
    usleep(1);
    //printf("%ld\n", uncached_time);

    tmp--;
  }

  printf("Cached time = %ld\n", cached_time/round);
  printf("Uncached time = %ld\n", uncached_time/round);
  return 0;
}
