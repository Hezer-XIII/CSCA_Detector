#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

unsigned int array1_size = 16;
uint8_t unused1[64];
uint8_t array1[160] = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16};
uint8_t unused2[64];
uint8_t array2[256 * 512];

char *secret = "The Magic Words are Squeamish Ossifrage."
               "The Magic Words are Squeamish Ossifrage."
               "The Magic Words are Squeamish Ossifrage."
               "The Magic Words are Squeamish Ossifrage."
               "The Magic Words are Squeamish Ossifrage."
               "The Magic Words are Squeamish Ossifrage."
               "The Magic Words are Squeamish Ossifrage."
               "The Magic Words are Squeamish Ossifrage."
               "The Magic Words are Squeamish Ossifrage."
               "The Magic Words are Squeamish Ossifrage.";

uint8_t temp = 0;

void victim_function(size_t x) {
  if (x < array1_size) {
    temp &= array2[array1[x] * 512];
  }
}

void readMemoryByte(size_t malicious_x){
  int tries, j;
  size_t training_x;

  for (tries=999; tries>0; tries--){
    training_x = tries % array1_size;
    for (j=29; j>=0; j--){
      int x = rand() % 32; // 50% to be in [0,16[ ; 50% to be in [16;32[
      victim_function(x);
    }
  }
}


int main(int argc, char const *argv[]) {
  /*First non-attack if behavior
    run victim_function with random x with a 50/50 to do a True/False on if
  */
  int i, len = 400;

  while (--len >= 0){
    readMemoryByte(0);
  }
  return 0;
}
