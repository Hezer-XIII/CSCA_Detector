#openssl=/home/hezer/Documents/School/ULB/MA2_2018-2019/Master_Thesis/test/lib/openssl
openssl=/home/Jeremy/CSCA_Detector/lib/openssl


all:	clean Flush+Flush_HalfKey Flush+Flush_FullKey Flush+Reload_HalfKey Flush+Reload_FullKey Prime+Probe_HalfKey Prime+Probe_FullKey spectre meltdown


noatt:	Prime+Probe_FullKey-noAttack
	#gcc src/Attack/spectre/normal_if_1.c -o dist/normal_if_1
	#gcc src/Attack/spectre/normal_if_2.c -o dist/normal_if_2
	#gcc src/Attack/spectre/spectre_only-if.c -o dist/spectre_only-if

threshold: Prime+Probe_Threshold Flush+Flush_Threshold Flush+Reload_Threshold


spectre:
	gcc src/Attack/Spectre/spectre.c -o dist/Spectre

meltdown:
	gcc src/Attack/Meltdown/meltdown.c -o dist/meltdown
	cp src/Attack/Meltdown/run.sh dist/run_meltdown.sh

Flush+Flush_HalfKey: src/Attack/Flush+Flush/Flush+Flush_HalfKey.cpp src/Attack/utils/cacheutils.h
	g++ -std=gnu++11 -O0 -o dist/$@ $< -I$(openssl) -L$(openssl) -lcrypto

Flush+Flush_FullKey: src/Attack/Flush+Flush/Flush+Flush_FullKey.cpp src/Attack/utils/cacheutils.h
	g++ -std=gnu++11 -O0 -o dist/$@ $< -I$(openssl) -L$(openssl) -lcrypto

Flush+Reload_HalfKey: src/Attack/Flush+Reload/Flush+Reload_HalfKey.cpp src/Attack/utils/cacheutils.h
	g++ -std=gnu++11 -O0 -o dist/$@ $< -I$(openssl) -L$(openssl) -lcrypto

Flush+Reload_FullKey: src/Attack/Flush+Reload/Flush+Reload_FullKey.cpp src/Attack/utils/cacheutils.h
	g++ -std=gnu++11 -O0 -o dist/$@ $< -I$(openssl) -L$(openssl) -lcrypto

Prime+Probe_FullKey: src/Attack/Prime+Probe/Prime+Probe_FullKey.cpp src/Attack/utils/cacheutils.h
	g++ -std=gnu++11 -O0 -o dist/$@ $< -I$(openssl) -L$(openssl) -lcrypto

Prime+Probe_HalfKey: src/Attack/Prime+Probe/Prime+Probe_HalfKey.cpp src/Attack/utils/cacheutils.h
	g++ -std=gnu++11 -O0 -o dist/$@ $< -I$(openssl) -L$(openssl) -lcrypto


Prime+Probe_FullKey-noAttack: src/noAttack/Prime+Probe_FullKey-noAttack.cpp src/Attack/utils/cacheutils.h
	g++ -std=gnu++11 -O0 -o dist/$@ $< -I$(openssl) -L$(openssl) -lcrypto


Prime+Probe_Threshold: src/Attack/Threshold/Prime+Probe_Threshold.cpp src/Attack/utils/cacheutils.h
	g++ -std=gnu++11 -O0 -g -o dist/$@ $< -I$(openssl) -L$(openssl) -lcrypto

Flush+Flush_Threshold: src/Attack/Threshold/Flush+Flush_Threshold.cpp src/Attack/utils/cacheutils.h
	g++ -std=gnu++11 -O0 -g -o dist/$@ $< -I$(openssl) -L$(openssl) -lcrypto

Flush+Reload_Threshold: src/Attack/Threshold/Flush+Reload_Threshold.cpp src/Attack/utils/cacheutils.h
	g++ -std=gnu++11 -O0 -g -o dist/$@ $< -I$(openssl) -L$(openssl) -lcrypto

Hit_Miss_Ratio: src/Attack/Threshold/Hit_Miss_Ratio.cpp src/Attack/utils/cacheutils.h
	g++ -std=gnu++11 -O0 -g -o dist/$@ $< -I$(openssl) -L$(openssl) -lcrypto



clean:
	rm -f dist/*


