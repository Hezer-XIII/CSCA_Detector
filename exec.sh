#!/bin/bash
#export LD_LIBRARY_PATH="/home/hezer/Desktop/openssl"
#export LD_LIBRARY_PATH="/home/hezer/Documents/School/ULB/MA2_2018-2019/Master_Thesis/test/lib/openssl"
export LD_LIBRARY_PATH="/home/Jeremy/CSCA_Detector/lib/openssl"


if [[ $(cat /proc/sys/kernel/perf_event_paranoid) != '-1' ]]
then
	echo 'event paranoid changed'
	sudo sh -c 'echo -1 >/proc/sys/kernel/perf_event_paranoid'
fi


#############################################
# Connect to the VPN
if [ $1 == 'ssh' ]
then
	ssh root@103.4.94.110 -p 2234
fi

###############################################################################
## Attacks
if [ $1 == 'FFhalf' ]
then
	./dist/Flush+Flush_HalfKey
fi

if [ $1 == 'FFfull' ]
then
	./dist/Flush+Flush_FullKey
fi

if [ $1 == 'FRhalf' ]
then
	./dist/Flush+Reload_HalfKey
fi

if [ $1 == 'FRfull' ]
then
	./dist/Flush+Reload_FullKey
fi

if [ $1 == 'PPhalf' ]
then
	./dist/Prime+Probe_HalfKey
fi

if [ $1 == 'PPfull' ]
then
	./dist/Prime+Probe_FullKey
fi

if [ $1 == 'spectre' ]
then
	./dist/Spectre
fi

if [ $1 == 'meltdown' ]
then
	./dist/run_meltdown.sh
fi


###############################################################################
## No Attacks
if [ $1 == 'PPNo' ]
then
	./dist/Prime+Probe_FullKey-noAttack
fi

###############################################################################
## Utils
if [ $1 == 'ps' ]
then
	ps  xao pid,ppid,pgid,sid,comm
fi

if [ $1 == 'threshold' ]
then
	#./dist/Prime+Probe_Threshold
	./dist/Flush+Flush_Threshold
	./dist/Flush+Reload_Threshold
fi


