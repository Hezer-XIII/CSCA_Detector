# Run Cache Attacks

1. Put the OpenSSL library in "code/lib". (extract the .tar.gz)  
   You can put OpenSSL anywhere, just remember to write the correct path.  
2. Build the openSSL library. You can use "lib/recompile.sh", just put this script into openSSL folder.  
3. Extract the t-Tables addresses (with "lib/recompile.sh") and put them into "src/Attack/utils/addr.h"  
4. Change the openSSL library location in "src/Attack/utils/addr.h"  
5. Modify the LD_LIBRARY_PATH in exec.sh  
6. Modify the openssl address in Makefile  
7. Modify the openssl address in "src/Attack/[Attack_Name]/[Attack_Name].cpp.  
   Flush+Flush_FullKey : Line 119.  
   Flush+Flush_HalfKey : Line 66.  
   Flush+Reload_FullKey : Line 120.   
   Flush+Reload_HalfKey : Line 64.  
   Prime+Probe_FullKey : Line 278.  
   Prime+Probe_HalfKey : Line 225.  
8. Change the Threshold value (see https://github.com/IAIK/flush_flush for more detail).  
   Flush+Flush_FullKey : Line 38.  
   Flush+Flush_HalfKey : Line 16.  
   Flush+Reload_FullKey : Line 38.   
   Flush+Reload_HalfKey : Line 17.  
   Prime+Probe_FullKey : Line 105.  
   Prime+Probe_HalfKey : Line 38.  
9. make (all) or make attack  
10. ./exec [attack_name] (ex: FRfull)  

# Run Spectre & Meltdown

1. make
2. ./exect spectre
   ./exect meltdown

# Detection

1. Create attacking and non-attacking data set (see src/main.py)  
2. sudo python3 src/main.py  


# Other information

- For more information : JeremyBricq.IT@gmail.com  
